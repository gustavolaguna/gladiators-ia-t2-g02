from .State import State
from random import randint
import numpy
import math
import random
import datetime, time
import operator
import os
from ctypes import c_longlong as ll

class Controller:

	def __init__(self, load, state):
		self.state = state
		self.init_table_Q(load, state)


    # TODO: carrega a tabela Q de um arquivo (se load!=None, entao load ira conter o nome do arquivo a ser carregado), 
    # ou, caso load==None, a funcao de inicializar uma tabela Q manualmente.
    # Dica: a tabela Q possui um valor para cada possivel par de estado e acao. Cada objeto do tipo State possui um id unico 
    # (calculado por State.get_state_id), o qual pode ser usado para indexar a sua tabela Q, juntamente com o indice da acao. 
    # Para criacao da tabela Q, pode ser importante saber o numero total de estados do sistema. Isso dependera de quantas features 
    # voce utilizar e em quantos niveis ira discretiza-las (ver arquivo State.py para mais detalhes). O numero total de
    # estados do sistema pode ser obtido atraves do metodo State.get_n_states.
    # Uma lista completa com os estados propriamente ditos pode ser obtida atraves do metodo State.states_list.
	def init_table_Q(self,load, state):
		self.q_table = numpy.zeros((4, 5, 5))
		if load != None:
			f = open(load, 'r')

			# action - prox_enemy - prox_arrow	
			index = 0
			for k in range(0, 4):
				for i in range(0, 5):
					for j in range(0, 5):
						try:
							self.q_table[k][i][j] = float(f.readline())
						except ValueError:
							print "Reading (%s), expected real on line %d, found '%s'." % (load, index, i)
							exit()
						index += 1

	# TODO: salvar a tabela Q aprendida para um arquivo--para posteriormente poder ser lida por init_table_Q
	def save_table_Q(self, episode, state):

		#Escrever nesse arquivo que esta sendo criado na pasta params
		if episode > 0 and episode % 10 == 0:
			if not os.path.exists("./params"):
				os.makedirs("./params")
			out = open("./params/%s.txt" % datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S'), "w+")

			for k in range(0, 4):
				for i in range(0, 5):
					for j in range(0, 5):
						out.write(str(self.q_table[k][i][j]))
						out.write('\n')

	# TODO: funcao que calcula recompensa a cada passo
	# Recebe como o parametro a acao executada, o estado anterior e posterior a execucao dessa acao,
	# o numero de passos desde o inicio do episodio, e um booleano indicando se o episodio acabou apos a execucao da acao.
	# Caso o episodio tenha terminado, o ultimo parametro especifica como ele terminou (IA "won", IA "lost", "draw" ou "collision")
    # Todas essas informacoes podem ser usadas para determinar que recompensa voce quer dar para o agente nessa situacao
	def compute_reward(self, action, prev_state, curr_state, nsteps, isEpisodeOver, howEpisodeEnded):
        # Abaixo, um exemplo de funcao de recompensa super simples (mas provavelmente nao muito efetiva)
		if isEpisodeOver:
			if howEpisodeEnded == "won":
				return 100
			elif howEpisodeEnded == "lost":
				return -100
			elif howEpisodeEnded == "draw" or howEpisodeEnded == "collision":
				return -50
		else:
			estimatedReward = 0
			prev_features = prev_state.get_state()
			curr_features = curr_state.get_state()

			'''
			if action == 1:
				if prev_features[0] == 0:
					estimatedReward = estimatedReward + 5	
				else:
					estimatedReward = estimatedReward - 5		
			if action == 2:
				estimatedReward = -5
			if action == 3:
				estimatedReward = -2
			if action == 4:
				if prev_features[0] > 0:
					estimatedReward = estimatedReward + 5
				else:
					estimatedReward = estimatedReward - 5
			return estimatedReward
			'''
			if action == 1:
				if prev_features[0] == 0:
					estimatedReward = estimatedReward + 10	
				else:
					estimatedReward = estimatedReward - 10
			if action == 2:
				if prev_features[0] > 0:
					estimatedReward = estimatedReward + 10
				else:
					estimatedReward = estimatedReward - 10
			if action == 3:
				estimatedReward = -5
			if action == 4:
				if prev_features[0] > 0:
					estimatedReward = estimatedReward + 50
				else:
					estimatedReward = estimatedReward - 50
			return estimatedReward
			


	# TODO: Deve consultar a tabela Q e escolher uma acao de acordo com a politica de exploracao
	# Retorna 1 caso a acao desejada seja direita, 2 caso seja esquerda, 3 caso seja nula, e 4 caso seja atirar
	def take_action(self, state):
		right_value = self.q_table[0][state.get_state()[0]][state.get_state()[1]]
		left_value = self.q_table[1][state.get_state()[0]][state.get_state()[1]]
		null_value = self.q_table[2][state.get_state()[0]][state.get_state()[1]]
		shoot_value = self.q_table[3][state.get_state()[0]][state.get_state()[1]]
		
		# boltzmann distribution
		def boltzmann(v, t, v1, v2, v3, v4):
		
			if v > 680*t:
				v = 680*t
			if v1 > 680*t:
				v1 = 680*t
			if v2 > 680*t:
				v2 = 680*t
			if v3 > 680*t:
				v3 = 680*t
			if v4 > 680*t:
				v4 = 680*t
			return (numpy.exp(v/t))/((numpy.exp(v1/t))+(numpy.exp(v2/t))+(numpy.exp(v3/t))+(numpy.exp(v4/t)))
		
		prob_right = boltzmann(right_value, 1, right_value, left_value, null_value, shoot_value)
		prob_left = boltzmann(left_value, 1, right_value, left_value, null_value, shoot_value) + prob_right
		prob_null = boltzmann(null_value, 1, right_value, left_value, null_value, shoot_value) + prob_left
		prob_shoot = boltzmann(shoot_value, 1, right_value, left_value, null_value, shoot_value) + prob_null
		
		chosen = numpy.random.uniform(0,1)
		if chosen <= prob_right:
			return 1
		elif chosen <= prob_left:
			return 2
		elif chosen <= prob_null:
			return 3
		else:
			return 4

		''' # greedy distribution
		n = numpy.random.uniform(0,1)
		if n < 0.1:
			if n < 0.025:
				return 1
			if n < 0.05:
				return 2
			if n < 0.075:
				return 3
			return 4
		else:
			m = max(right_value, left_value, null_value, shoot_value)
			if m == right_value:
				return 1
			if m == left_value:
				return 2
			if m == null_value:
				return 3
			if m == shoot_value:
				return 4'''



	# TODO: Implementa a regra de atualziacao do Q-Learning.
	# Recebe como o parametro a acao executada, o estado anterior e posterior a execucao dessa acao,
	# a recompensa obtida e um booleano indicando se o episodio acabou apos a execucao da acao
	def updateQ(self, action, prev_state, curr_state, reward, isEpisodeOver):
		prev_state_value = self.q_table[action-1][prev_state.get_state()[0]][prev_state.get_state()[1]]
		curr_right = self.q_table[0][curr_state.get_state()[0]][curr_state.get_state()[1]]
		curr_left = self.q_table[1][curr_state.get_state()[0]][curr_state.get_state()[1]]
		curr_null = self.q_table[2][curr_state.get_state()[0]][curr_state.get_state()[1]]
		curr_shoot = self.q_table[3][curr_state.get_state()[0]][curr_state.get_state()[1]]
		
		max_q = max(curr_left, curr_null, curr_right, curr_shoot)
		alpha = 0.5
		gamma = 0.5
		'''
		print('q ',max_q)
		print('curr_left',curr_left)
		print('curr_null',curr_null)
		print('curr_right',curr_right)
		print('curr_shoot',curr_shoot)
		print('reward:',reward)
		print('action: ',action)
		'''
		
		self.q_table[action-1][prev_state.get_state()[0]][prev_state.get_state()[1]] = (1-alpha)*prev_state_value + alpha*(reward+gamma*max_q)
		#print(self.q_table[action-1][prev_state.get_state()[0]][prev_state.get_state()[1]])